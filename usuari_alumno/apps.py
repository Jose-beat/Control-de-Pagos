from django.apps import AppConfig


class UsuariAlumnoConfig(AppConfig):
    name = 'usuari_alumno'
